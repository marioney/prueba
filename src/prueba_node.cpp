#include <prueba/prueba_jorge.hpp>
int main(int argc, char **argv)
{
  // Initialize ROS
  ros::init(argc, argv, "prueba_jorge");
  // Give time to load all other components so the filter
  // can be included in the launcher
  ROS_INFO("Pose EKF: -------------- Waiting for other process to start");
  sleep(2);
  //ROS_INFO("Pose EKF: -------------- About to Start Pose EKF");
  //sleep(2);

  // create filter class
  PruebaJorge my_prueba;

  ros::spin();

  return 0;
}
